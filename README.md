# IoT Home Automation Android Application
The project is about distributed home Automation for controlling and monitoring various devices with Android app based on IoT.
Requirements

# Architectural design
![final_architecture](/uploads/5dc950b591cb29fe47c6bcdf1382ef20/final_architecture.png)


# Android App
Android Homescreen</br>
![HomeScreen](/uploads/bcbe0c8765d8d414042a4db6daebdae8/HomeScreen.png)

Switching on/off:
![Screenshot_1523233779](/uploads/f74ad9d9590e384ac64104e12a192a2f/Screenshot_1523233779.png)

