package com.iothomeautomation.iothomeautomation.Util;

/**
 * Created by arunteja on 2-02-2018.
 */

public class ServerAPI {
    public static final String URL_DATA = "http://192.168.1.6/crud_android/rfid_view.php";
    public static final String URL_INSERT = "http://192.168.1.6/crud_android/rfid_add_f.php";
    public static final String URL_DELETE = "http://192.168.1.6/crud_android/rfid_del_f.php";
    public static final String URL_UPDATE = "http://192.168.1.6/crud_android/rfid_update.php";
}
