package com.iothomeautomation.iothomeautomation;

/**
 * Created by arunteja on 2-02-2018.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    Button buttonTemp;
    Button buttonRF;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        buttonRF = findViewById(R.id.btnRFID);
        buttonTemp = findViewById(R.id.btnTempHumid);

        buttonTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, listviewTemp.class);
                startActivity(intent);
            }

        });
        buttonRF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, RFView.class);
                startActivity(intent);
            }
        });
    }
}